* 组长 王顺（题目：Tensorflow object detection API 物体检测手机APP）
* 组员及各自分工：

1.王顺（WangShun），调试程序，创建了仓库，推送了新的 分支 master 到 仓库，邀请成员加入仓库， 并更新README.md，使用Git GUI上传项目源代码，并上传UML构件图以及自己的课程设计电子版。

2.张雪萍（zhangxueping），调试程序，加入了仓库，并上传UML协作图以及自己的课程设计电子版。

3.张梦月（zhangmengyue），调试程序，加入了 仓库，并上传UML顺序图以及自己的课程设计电子版。

4.孟祥凤（Mengxiangfeng），调试程序，加入了 仓库，并上传UML活动图以及自己的课程设计电子版。

5.李先亮（lixianliang1997），调试程序，加入了仓库，并上传UML用例图以及自己的课程设计电子版。

6.战雅洁（zhanyajie），调试程序，加入了 仓库，并上传UML状态图以及自己的课程设计电子版。

7.鲍海霞（baohaixia），调试程序，加入了仓库，并上传UML类图以及自己的课程设计电子版。

* 进度计划

1.10月份：搜集资料，了解深度学习知识以及Tensorflow

2.11月份：配置程序开发环境，共同调试程序代码

3.12月份：调试程序，上传程序代码

4.1月份：画出UML图

* 项目地址
  https://gitee.com/WangShun1997/UML_SHUN